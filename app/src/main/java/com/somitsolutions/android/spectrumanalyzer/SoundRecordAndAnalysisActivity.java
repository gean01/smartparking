package com.somitsolutions.android.spectrumanalyzer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import ca.uol.aig.fftpack.RealDoubleFFT;
import weka.classifiers.Classifier;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

import static android.content.ContentValues.TAG;


public class SoundRecordAndAnalysisActivity extends Activity implements OnClickListener{

	int frequency = 8000;
    int channelConfiguration = AudioFormat.CHANNEL_CONFIGURATION_MONO;
    int audioEncoding = AudioFormat.ENCODING_PCM_16BIT;

    AudioRecord audioRecord;
    private RealDoubleFFT transformer;
    int blockSize;// = 256;
    Button startStopButton;
    boolean started = false;
    boolean CANCELLED_FLAG = false;
    EditText edtOutput;
    TextView txtOutput;
    Button saveButton;
    double [][] savedProgress;


    RecordAudio recordTask;
    ImageView imageViewDisplaySectrum;
    MyImageView imageViewScale;
    Bitmap bitmapDisplaySpectrum;

    Canvas canvasDisplaySpectrum;

    Paint paintSpectrumDisplay;
    Paint paintScaleDisplay;
    static SoundRecordAndAnalysisActivity mainActivity;
    LinearLayout main;
    int width;
    int height;
    int left_Of_BimapScale;
    int left_Of_DisplaySpectrum;
    private final static int ID_BITMAPDISPLAYSPECTRUM = 1;
    private final static int ID_IMAGEVIEWSCALE = 2;

    private Classifier cls = null;



    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Display display = getWindowManager().getDefaultDisplay();
    	//Point size = new Point();
    	//display.get(size);
    	width = display.getWidth();
    	height = display.getHeight();

        blockSize = 256;



    }

    @Override
	public void onWindowFocusChanged (boolean hasFocus) {
    	//left_Of_BimapScale = main.getC.getLeft();
    	MyImageView  scale = (MyImageView)main.findViewById(ID_IMAGEVIEWSCALE);
    	ImageView bitmap = (ImageView)main.findViewById(ID_BITMAPDISPLAYSPECTRUM);
    	left_Of_BimapScale = scale.getLeft();
    	left_Of_DisplaySpectrum = bitmap.getLeft();
    }
    private class RecordAudio extends AsyncTask<Void, double[], Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {

            int bufferSize = AudioRecord.getMinBufferSize(frequency,
                    channelConfiguration, audioEncoding);
            audioRecord = new AudioRecord(
                    MediaRecorder.AudioSource.DEFAULT, frequency,
                    channelConfiguration, audioEncoding, bufferSize);
            int bufferReadResult;
            short[] buffer = new short[blockSize];
            double[] toTransform = new double[blockSize];
            try {
                audioRecord.startRecording();
            } catch (IllegalStateException e) {
                Log.e("Recording failed", e.toString());

            }
            while (started) {

                if (isCancelled() || (CANCELLED_FLAG == true)) {

                    started = false;
                    //publishProgress(cancelledResult);
                    Log.d("doInBackground", "Cancelling the RecordTask");
                    break;
                } else {
                    bufferReadResult = audioRecord.read(buffer, 0, blockSize);

                    for (int i = 0; i < blockSize && i < bufferReadResult; i++) {
                        toTransform[i] = (double) buffer[i] / 32768.0; // signed 16 bit
                    }

                    transformer.ft(toTransform);

                    publishProgress(toTransform);

                }

            }
            return true;
        }

        private void wekaExample(){
            try {
                int NUMBER_OF_ATTRIBUTES = 7; // 6 + 1 class
                int NUMBER_OF_INSTANCES = 1;
                Attribute Attribute1 = new Attribute("nameOfAttribute_1");
                Attribute Attribute2 = new Attribute("nameOfAttribute_2");
                Attribute Attribute3 = new Attribute("nameOfAttribute_3");
                Attribute Attribute4 = new Attribute("nameOfAttribute_4");
                Attribute Attribute5 = new Attribute("nameOfAttribute_5");
                Attribute Attribute6 = new Attribute("nameOfAttribute_6");
                Attribute AttributeClass = new Attribute("nameOfClassAttribute");

                FastVector fvWekaAttributes = new FastVector(NUMBER_OF_ATTRIBUTES);
                fvWekaAttributes.addElement(Attribute1);
                fvWekaAttributes.addElement(Attribute2);
                fvWekaAttributes.addElement(Attribute3);
                fvWekaAttributes.addElement(Attribute4);
                fvWekaAttributes.addElement(Attribute5);
                fvWekaAttributes.addElement(Attribute6);
                fvWekaAttributes.addElement(AttributeClass);

                Instances trainingSet = new Instances("Rel", fvWekaAttributes, NUMBER_OF_INSTANCES);
                trainingSet.setClassIndex(6); // Set class index

                // Fill the training set with one instance (weka.core.Instance)
                Instance iExample = new DenseInstance(7);
                iExample.setValue(Attribute1, 124);
                iExample.setValue(Attribute2, 255);
                iExample.setValue(Attribute3, 5999);
                iExample.setValue(Attribute4, 255);
                iExample.setValue(Attribute5, 17);
                iExample.setValue(Attribute6, 129);

                trainingSet.add(iExample);
                Classifier cls = (Classifier) weka.core.SerializationHelper.read( getAssets().open("cpu.model") );
                double prediction = cls.classifyInstance(trainingSet.instance( 0 ));//perform your prediction
                int x = 1;

            }catch (Exception e){
            }


        }

        private double weka( int a1, int a2, int a3, int a4, int a5, int a6 ) throws  Exception{

            Attribute Attribute1 = new Attribute("a1");
            Attribute Attribute2 = new Attribute("a2");
            Attribute Attribute3 = new Attribute("a3");
            Attribute Attribute4 = new Attribute("a4");
            Attribute Attribute5 = new Attribute("a5");
            Attribute Attribute6 = new Attribute("a6");
            Attribute AttributeClass = new Attribute("ac");

            FastVector fvWekaAttributes = new FastVector(7);
            fvWekaAttributes.addElement(Attribute1);
            fvWekaAttributes.addElement(Attribute2);
            fvWekaAttributes.addElement(Attribute3);
            fvWekaAttributes.addElement(Attribute4);
            fvWekaAttributes.addElement(Attribute5);
            fvWekaAttributes.addElement(Attribute6);
            fvWekaAttributes.addElement(AttributeClass);

            Instances trainingSet = new Instances("Rel", fvWekaAttributes, 10);
            // Set class index
            trainingSet.setClassIndex(6);

            //Now, let’s fill the training set with one instance (weka.core.Instance):
            // Create the instance
            Instance iExample = new DenseInstance(7);
            iExample.setValue((Attribute)fvWekaAttributes.elementAt(0), a1);
            iExample.setValue((Attribute)fvWekaAttributes.elementAt(1), a2);
            iExample.setValue((Attribute)fvWekaAttributes.elementAt(2), a3);
            iExample.setValue((Attribute)fvWekaAttributes.elementAt(3), a4);
            iExample.setValue((Attribute)fvWekaAttributes.elementAt(4), a5);
            iExample.setValue((Attribute)fvWekaAttributes.elementAt(5), a6);


            trainingSet.add(iExample);

            if( cls == null ){
                cls = (Classifier) weka.core.SerializationHelper.read( getAssets().open("cpu.model") );
            }


            // which instance to predict class value
            int s1 = 0;

            //perform your prediction
            double prediction = cls.classifyInstance(trainingSet.instance( s1 ));

            //get the name of the class value
            String class_name = trainingSet.classAttribute().value(( int)prediction );

            //System.out.println("The predicted value of instance " + Integer.toString(s1) + ": " + prediction );
            return prediction;

        }

        @Override
        protected void onProgressUpdate(double[]...progress) {


            final int a1 = (int)progress[0][0];
            final int a2 = (int)progress[0][1];
            final int a3 = (int)progress[0][2];
            final int a4 = (int)progress[0][3];
            final int a5 = (int)progress[0][4];
            final int a6 = (int)progress[0][5];

            savedProgress = progress;




            runOnUiThread(new Runnable() {
                public void run() {
                    int prediction = 0;

                    try {
                        prediction = (int) weka(a1,a2,a3,a4,a5,a6);
                        wekaExample();
                    }catch( Exception e ){

                    }
                    txtOutput.setText( "Distância: " + String.valueOf( prediction ) + " cm" );
                }
            });



            Log.d("Test:", Integer.toString(progress[0].length));

            Log.e("RecordingProgress", "Displaying in width = " + width);


                if (width > 512) {
                    for (int i = 0; i < progress[0].length; i++) {
                        int x = 2 * i;
                        int downy = (int) (150 - (progress[0][i] * 10));
                        int upy = 150;
                        //canvasDisplaySpectrum.drawLine(x, downy, x, upy, paintSpectrumDisplay);
                    }

                    imageViewDisplaySectrum.invalidate();
                } else {
                    for (int i = 0; i < progress[0].length; i++) {
                        int x = i;
                        int downy = (int) (150 - (progress[0][i] * 10));
                        int upy = 150;
                        //canvasDisplaySpectrum.drawLine(x, downy, x, upy, paintSpectrumDisplay);
                    }

                    imageViewDisplaySectrum.invalidate();
                }





        }
        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
        	try{
            	audioRecord.stop();
            }
            catch(IllegalStateException e){
            	Log.e("Stop failed", e.toString());

            }

            canvasDisplaySpectrum.drawColor(Color.BLACK);
            imageViewDisplaySectrum.invalidate();

            }
       }

        protected void onCancelled(Boolean result){

            try{
                audioRecord.stop();
            }
            catch(IllegalStateException e){
                Log.e("Stop failed", e.toString());

            }
           /* //recordTask.cancel(true);
            Log.d("FFTSpectrumAnalyzer","onCancelled: New Screen");
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
*/
        }

        public void onClick(View v) {
        if (started == true) {
                //started = false;
                CANCELLED_FLAG = true;
                //recordTask.cancel(true);
                try{
                    audioRecord.stop();
                }
                catch(IllegalStateException e){
                    Log.e("Stop failed", e.toString());

                }
                startStopButton.setText("Escutar");

                canvasDisplaySpectrum.drawColor(Color.BLACK);

            }

        else {
                started = true;
                CANCELLED_FLAG = false;
                startStopButton.setText("Parar");
                recordTask = new RecordAudio();
                recordTask.execute();
        }

     }
        static SoundRecordAndAnalysisActivity getMainActivity(){

            return mainActivity;
        }

        public void onStop(){
        	super.onStop();
        	/* try{
                 audioRecord.stop();
             }
             catch(IllegalStateException e){
                 Log.e("Stop failed", e.toString());

             }*/
        	recordTask.cancel(true);
            Intent intent = new Intent(Intent.ACTION_MAIN);
        	intent.addCategory(Intent.CATEGORY_HOME);
        	intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        	startActivity(intent);
        }

        private String getModel(){
            return "";
        }



        public void onStart(){

        	super.onStart();
        	main = new LinearLayout(this);
        	main.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,android.view.ViewGroup.LayoutParams.MATCH_PARENT));
        	main.setOrientation(LinearLayout.VERTICAL);
        	setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        	requestWindowFeature(Window.FEATURE_NO_TITLE);
        	getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

        	transformer = new RealDoubleFFT(blockSize);

            imageViewDisplaySectrum = new ImageView(this);
            if(width > 512){
            	bitmapDisplaySpectrum = Bitmap.createBitmap((int)512,(int)300,Bitmap.Config.ARGB_8888);
            }
            else{
            	 bitmapDisplaySpectrum = Bitmap.createBitmap((int)256,(int)150,Bitmap.Config.ARGB_8888);
            }
            LinearLayout.LayoutParams layoutParams_imageViewScale = null;
            //Bitmap scaled = Bitmap.createScaledBitmap(bitmapDisplaySpectrum, 320, 480, true);
            canvasDisplaySpectrum = new Canvas(bitmapDisplaySpectrum);
            //canvasDisplaySpectrum = new Canvas(scaled);
            paintSpectrumDisplay = new Paint();
            paintSpectrumDisplay.setColor(Color.GREEN);
            imageViewDisplaySectrum.setImageBitmap(bitmapDisplaySpectrum);
            if(width >512){
            	//imageViewDisplaySectrum.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT));
            	LinearLayout.LayoutParams layoutParams_imageViewDisplaySpectrum=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                ((MarginLayoutParams) layoutParams_imageViewDisplaySpectrum).setMargins(100, 600, 0, 0);
                imageViewDisplaySectrum.setLayoutParams(layoutParams_imageViewDisplaySpectrum);
                layoutParams_imageViewScale= new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                //layoutParams_imageViewScale.gravity = Gravity.CENTER_HORIZONTAL;
                ((MarginLayoutParams) layoutParams_imageViewScale).setMargins(100, 20, 0, 0);

            }

            else if ((width >320) && (width<512)){
            	LinearLayout.LayoutParams layoutParams_imageViewDisplaySpectrum=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                ((MarginLayoutParams) layoutParams_imageViewDisplaySpectrum).setMargins(60, 250, 0, 0);
               //layoutParams_imageViewDisplaySpectrum.gravity = Gravity.CENTER_HORIZONTAL;
                imageViewDisplaySectrum.setLayoutParams(layoutParams_imageViewDisplaySpectrum);

            	//imageViewDisplaySectrum.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT));
            	layoutParams_imageViewScale=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            	((MarginLayoutParams) layoutParams_imageViewScale).setMargins(60, 20, 0, 100);
            	//layoutParams_imageViewScale.gravity = Gravity.CENTER_HORIZONTAL;
            }

            else if (width < 320){
            	/*LinearLayout.LayoutParams layoutParams_imageViewDisplaySpectrum=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                ((MarginLayoutParams) layoutParams_imageViewDisplaySpectrum).setMargins(30, 100, 0, 100);
                imageViewDisplaySectrum.setLayoutParams(layoutParams_imageViewDisplaySpectrum);*/
            	imageViewDisplaySectrum.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT));
            	layoutParams_imageViewScale=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            	//layoutParams_imageViewScale.gravity = Gravity.CENTER;
            }
            imageViewDisplaySectrum.setId(ID_BITMAPDISPLAYSPECTRUM);
            main.addView(imageViewDisplaySectrum);



            Display display = getWindowManager().getDefaultDisplay();
            int width = display.getWidth();


            // Gean
            edtOutput = new EditText(this);
            edtOutput.setFocusable(true);
            edtOutput.setFocusableInTouchMode(true);
            edtOutput.setWidth(width);
            edtOutput.setInputType(InputType.TYPE_CLASS_NUMBER);
            edtOutput.setHint("Distância medida (cm)");
            LinearLayout a = new LinearLayout(this);
            a.addView(edtOutput);
            a.setOrientation(LinearLayout.VERTICAL);
            main.addView(a);



            saveButton = new Button(this);
            saveButton.setText("Salvar");
            saveButton.setWidth(width);



            saveButton.setOnClickListener( new OnClickListener() {

                public void onClick(View v) {
                    if( savedProgress == null ){
                        return;
                    }

                    //saveButton.setText("SALVOU!");
                    List<Double> savedProgessAsIterable = new ArrayList<Double>();

                    for( int i = 0; i < savedProgress[0].length; i++ ){
                        savedProgessAsIterable.add( savedProgress[0][i] );
                    }
                    String distanceAsStr = edtOutput.getText().toString();
                    savedProgessAsIterable.add( Double.parseDouble( distanceAsStr ) );


                    String progressSeparatedByComma = TextUtils.join(", ", savedProgessAsIterable);

                    File log = new File(Environment.getExternalStorageDirectory(), "dados_sensor.txt");
                    try {
                        BufferedWriter out = new BufferedWriter(new FileWriter( log.getAbsolutePath(), false));
                        out.write( progressSeparatedByComma  );
                        out.write(" : \n");
                    } catch (Exception e) {
                        Log.e(TAG, "Error opening Log.", e);
                    }

                    String filename = "dados_sensor.txt";
                    File filex = new File(Environment.getExternalStorageDirectory(), filename);
                    FileOutputStream fos;
                    byte[] data = new String( progressSeparatedByComma ).getBytes();
                    try {
                        fos = new FileOutputStream(filex);
                        fos.write(data);
                        fos.flush();
                        fos.close();
                        // handle exception
                    } catch (Exception e) {
                        // handle exception
                    }


                }
            });



            LinearLayout c = new LinearLayout(this);
            c.addView(saveButton);
            c.setOrientation(LinearLayout.VERTICAL);
            main.addView(c);



            txtOutput = new TextView(this);
            txtOutput.setText("Distância: N/A");
            LinearLayout b = new LinearLayout(this);
            b.addView(txtOutput);
            b.setOrientation(LinearLayout.VERTICAL);
            main.addView(b);




            imageViewScale = new MyImageView(this);
            imageViewScale.setLayoutParams(layoutParams_imageViewScale);
            imageViewScale.setId(ID_IMAGEVIEWSCALE);

            //imageViewScale.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT));
            main.addView(imageViewScale);

            startStopButton = new Button(this);
            startStopButton.setText("Escutar");
            startStopButton.setOnClickListener(this);
            startStopButton.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT));

            main.addView(startStopButton);

            setContentView(main);

            mainActivity = this;

        }
        @Override
        public void onBackPressed() {
        	super.onBackPressed();

        	 try{
                 audioRecord.stop();
             }
             catch(IllegalStateException e){
                 Log.e("Stop failed", e.toString());

             }
        	 recordTask.cancel(true);
        	Intent intent = new Intent(Intent.ACTION_MAIN);
        	intent.addCategory(Intent.CATEGORY_HOME);
        	intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        	startActivity(intent);
        }

        @Override
        protected void onDestroy() {
            // TODO Auto-generated method stub
            super.onDestroy();
            try{
                audioRecord.stop();
            }
            catch(IllegalStateException e){
                Log.e("Stop failed", e.toString());

            }
            recordTask.cancel(true);
            Intent intent = new Intent(Intent.ACTION_MAIN);
        	intent.addCategory(Intent.CATEGORY_HOME);
        	intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        	startActivity(intent);
        }
        //Custom Imageview Class
        public class MyImageView extends ImageView {
        	Paint paintScaleDisplay;
        	Bitmap bitmapScale;
        	Canvas canvasScale;
        	public MyImageView(Context context) {
        		super(context);
        		// TODO Auto-generated constructor stub
        		if(width >512){
        			bitmapScale = Bitmap.createBitmap((int)512,(int)50,Bitmap.Config.ARGB_8888);
                }
        		else{
        			bitmapScale =  Bitmap.createBitmap((int)256,(int)50,Bitmap.Config.ARGB_8888);
        		}

        		paintScaleDisplay = new Paint();
        		paintScaleDisplay.setColor(Color.WHITE);
                paintScaleDisplay.setStyle(Paint.Style.FILL);

                canvasScale = new Canvas(bitmapScale);

                setImageBitmap(bitmapScale);
                invalidate();
        	}
        	@Override
            protected void onDraw(Canvas canvas)
            {
                // TODO Auto-generated method stub
                super.onDraw(canvas);

                if(width > 512){
                	 //canvasScale.drawLine(0, 30,  512, 30, paintScaleDisplay);
                	for(int i = 0,j = 0; i< 512; i=i+128, j++){
                     	for (int k = i; k<(i+128); k=k+16){
                     		//canvasScale.drawLine(k, 30, k, 25, paintScaleDisplay);
                     	}
                     	//canvasScale.drawLine(i, 40, i, 25, paintScaleDisplay);
                     	String text = Integer.toString(j) + " KHz";
                     	//canvasScale.drawText(text, i, 45, paintScaleDisplay);
                     }
                	//canvas.drawBitmap(bitmapScale, 0, 0, paintScaleDisplay);
                }
                else if ((width >320) && (width<512)){
                	 //canvasScale.drawLine(0, 30, 0 + 256, 30, paintScaleDisplay);
                	 for(int i = 0,j = 0; i<256; i=i+64, j++){
                     	for (int k = i; k<(i+64); k=k+8){
                     		//canvasScale.drawLine(k, 30, k, 25, paintScaleDisplay);
                     	}
                     	//canvasScale.drawLine(i, 40, i, 25, paintScaleDisplay);
                     	String text = Integer.toString(j) + " KHz";
                     	//canvasScale.drawText(text, i, 45, paintScaleDisplay);
                     }
                	 //canvas.drawBitmap(bitmapScale, 0, 0, paintScaleDisplay);
                }

                else if (width <320){
               	 //canvasScale.drawLine(0, 30,  256, 30, paintScaleDisplay);
               	 for(int i = 0,j = 0; i<256; i=i+64, j++){
                    	for (int k = i; k<(i+64); k=k+8){
                    		//canvasScale.drawLine(k, 30, k, 25, paintScaleDisplay);
                    	}
                    	//canvasScale.drawLine(i, 40, i, 25, paintScaleDisplay);
                    	String text = Integer.toString(j) + " KHz";
                    	//canvasScale.drawText(text, i, 45, paintScaleDisplay);
                    }
               	 //canvas.drawBitmap(bitmapScale, 0, 0, paintScaleDisplay);
               }
            }
        }
}
    
